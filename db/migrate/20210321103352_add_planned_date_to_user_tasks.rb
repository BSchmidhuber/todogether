# frozen_string_literal: true

class AddPlannedDateToUserTasks < ActiveRecord::Migration[6.1]
  def change
    add_column :user_tasks, :planned_date, :datetime
  end
end
